# ![Node/Express/Mongoose Example App](project-logo.png)

[![Build Status](https://travis-ci.org/anishkny/node-express-realworld-example-app.svg?branch=master)](https://travis-ci.org/anishkny/node-express-realworld-example-app)

> ### NestJS (Express + Mongoose) codebase containing real world examples (CRUD, auth, advanced patterns, etc) that adheres to the [RealWorld](https://github.com/gothinkster/realworld-example-apps) API spec.


----------

# Getting started

## Installation

Clone the repository

    git clone https://kaminto@bitbucket.org/kaminto/nestjs-realworld-example-app.git

Switch to the repo folder

    cd nestjs-realworld-example-app
    
Install dependencies
    
    npm install

    
----------

## Database

The example codebase uses [Typeorm](http://typeorm.io/) with a mySQL database.

Create a new mysql database with the name `nestjsrealworld` (or the name you specified in the ormconfig.json)

In the typeorm [ormconfig.json](https://bitbucket.org/kaminto/nestjs-realworld-example-app/src/master/ormconfig.json) config file for database settings
 ``
    
Set mysql database settings in ormconfig.json

    {
      "type": "mysql",
      "host": "localhost",
      "port": 3306,
      "username": "your-mysql-username",
      "password": "your-mysql-password",
      "database": "nestjsrealworld",
      "entities": ["src/**/**.entity{.ts,.js}"],
      "synchronize": true
    }
    
Start local mysql server and create new database 'nestjsrealworld'

Upload [this](https://bitbucket.org/kaminto/nestjs-realworld-example-app/src/master/nestjsrealworld.sql) example database in your mysaql server.

----------

## NPM scripts

- `npm run start:dev` - Start application in development mode
- `npm run prestart:prod` - Building the application
- `npm run test` - run Jest test runner 
- `npm run start:prod` - Running the application production

----------

## API Specification

This application adheres to the api specifications set by the [Thinkster](https://github.com/gothinkster) team. This helps mix and match any backend with any other frontend without conflicts.

> [Full API Spec](https://github.com/gothinkster/realworld/tree/master/api)

More information regarding the project can be found here https://github.com/gothinkster/realworld

----------

## Start application

- `npm run start:dev`
- Open the api documentation with this link `http://localhost:3030/docs` in your favourite browser
- The base api for the application is  `http://localhost:3030/api` followed by the various endpoints found in the swagger documentation
----------

# Authentication
 
This applications uses JSON Web Token (JWT) to handle authentication. The token is passed with each request using the `Authorization` header with `Token` scheme. The JWT authentication middleware handles the validation and authentication of the token. Please check the following sources to learn more about JWT.

*** Authentication Header Example ***

`Authorization: Bearer "Token provided"`

- The `/user/` and `user/login` endpoint dont require the `Authorization`

----------
 
# Swagger API docs

This example repo uses the NestJS swagger module for API documentation. [NestJS Swagger](https://github.com/nestjs/swagger) - [www.swagger.io](https://swagger.io/)        