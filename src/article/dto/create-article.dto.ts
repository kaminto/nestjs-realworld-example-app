import { ApiModelProperty } from '@nestjs/swagger';

export class CreateArticleDto {
  @ApiModelProperty({ required: true })
  readonly title: string;
  @ApiModelProperty({ required: true })
  readonly description: string;
  @ApiModelProperty({ required: true })
  readonly body: string;
  @ApiModelProperty({ required: false })
  readonly tagList: string[];
}

// tslint:disable-next-line:max-classes-per-file
export class AssignCategoryDto {
  @ApiModelProperty({ required: true })
  readonly categoryId: number;
}
