import { ApiModelProperty } from '@nestjs/swagger';
export class CreateCommentDto {
  @ApiModelProperty({ required: true })
  readonly body: string;
}
