import {
  Get,
  Post,
  Body,
  Put,
  Delete,
  Param,
  Controller,
  UsePipes,
} from '@nestjs/common';
import { UserService } from './user.service';
import { UserRO } from './user.interface';
import { CreateUserDto, UpdateUserDto, LoginUserDto } from './dto';
import { HttpException } from '@nestjs/common/exceptions/http.exception';
import { User } from './user.decorator';
import { ValidationPipe } from '../shared/pipes/validation.pipe';

import { ApiUseTags,
  ApiBearerAuth,
  ApiOperation } from '@nestjs/swagger';

@ApiBearerAuth()
@ApiUseTags('user')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiOperation({ title: 'Get User' })
  @Get()
  async findMe(@User('email') email: string): Promise<UserRO> {
    return await this.userService.findByEmail(email);
  }

  @ApiOperation({ title: 'Update User' })
  @Put()
  async update(
    @User('id') userId: number,
    @Body() userData: UpdateUserDto,
  ) {
    return await this.userService.update(userId, userData);
  }

  @ApiOperation({ title: 'Create User' })
  @UsePipes(new ValidationPipe())
  @Post()
  async create(@Body() userData: CreateUserDto) {
    return this.userService.create(userData);
  }

  @ApiOperation({ title: 'Delete User' })
  @Delete(':email')
  async delete(@Param('email') email) {
    return await this.userService.delete(email);
  }

  @ApiOperation({ title: 'Login User' })
  @UsePipes(new ValidationPipe())
  @Post('login')
  async login(@Body() loginUserDto: LoginUserDto): Promise<UserRO> {
    // tslint:disable-next-line:variable-name
    const _user = await this.userService.findOne(loginUserDto);

    const errors = { User: ' not found' };
    // tslint:disable-next-line:curly
    if (!_user) throw new HttpException({ errors }, 401);

    const token = await this.userService.generateJWT(_user);
    const { email, username, bio, image } = _user;
    const user = { email, token, username, bio, image };
    return { user };
  }
}
