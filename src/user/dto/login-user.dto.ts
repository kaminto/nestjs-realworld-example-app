import { IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
export class LoginUserDto {
  @ApiModelProperty({ required: true })
  @IsNotEmpty()
  readonly email: string;
  @ApiModelProperty({ required: true })
  @IsNotEmpty()
  readonly password: string;
}
