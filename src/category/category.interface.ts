import { CategoryEntity } from './category.entity';

interface CategoryData {
  parentId: number;
  title: string;
  description: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface CategoryRO {
  category: CategoryEntity;
}

export interface CategoriesRO {
  categories: CategoryEntity[];
  categoriesCount: number;
}
