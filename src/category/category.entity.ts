import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    OneToOne,
    ManyToOne,
    OneToMany,
    JoinColumn,
    AfterUpdate,
    BeforeUpdate,
  } from 'typeorm';
import { ArticleEntity } from '../article/article.entity';
@Entity('category')
  export class CategoryEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    parentId: number;

    @Column()
    title: string;

    @Column({ default: '' })
    description: string;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    created: Date;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    updated: Date;

    @BeforeUpdate()
    updateTimestamp() {
      this.updated = new Date();
    }

    @OneToMany(type => ArticleEntity, article => article.category)
    articles: ArticleEntity[];

  }
