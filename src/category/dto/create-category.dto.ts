import { ApiModelProperty } from '@nestjs/swagger';
export class CreateCategoryDto {
    @ApiModelProperty({ required: true })
    readonly parentId: number;
    @ApiModelProperty({ required: true })
    readonly title: string;
    @ApiModelProperty({ required: true })
    readonly description: string;
  }
