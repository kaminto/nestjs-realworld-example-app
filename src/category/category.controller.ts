import {
    Get,
    Post,
    Body,
    Put,
    Delete,
    Query,
    Param,
    Controller,
} from '@nestjs/common';
import { Request } from 'express';
import { CategoryService } from './category.service';
import { CreateCategoryDto } from './dto/create-category.dto';
import { CategoriesRO, CategoryRO } from './category.interface';
import { User } from '../user/user.decorator';
import {
    ApiUseTags,
    ApiBearerAuth,
    ApiResponse,
    ApiOperation,
} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiUseTags('category')
@Controller('category')
export class CategoryController {
    constructor(private readonly categoryService: CategoryService) { }

    @ApiOperation({ title: 'Get all categories' })
    @ApiResponse({ status: 200, description: 'Return all categories.' })
    @Get()
    async findAll(@Query() query): Promise<CategoriesRO> {
        return await this.categoryService.findAll(query);
    }

    @ApiOperation({ title: 'Get Category by Id' })
    @Get(':categoryId')
    async findOne(@Param('categoryId') id): Promise<CategoryRO> {
        return await this.categoryService.findOne({ id });
    }

    @ApiOperation({ title: 'Create Category' })
    @ApiResponse({
        status: 200,
        description: 'The Category has been successfully created.',
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @Post()
    async create(
        @User('id') userId: number,
        @Body() categoryData: CreateCategoryDto,
    ) {
        return this.categoryService.create(userId, categoryData);
    }

    @ApiOperation({ title: 'Update Category' })
    @ApiResponse({
        status: 201,
        description: 'The category has been successfully updated.',
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @Put(':categoryId')
    async update(
        @Param() params,
        @Body() categoryData: CreateCategoryDto,
    ) {
        return this.categoryService.update(params.categoryId, categoryData);
    }

    @ApiOperation({ title: 'Delete Category' })
    @ApiResponse({
        status: 201,
        description: 'The category has been successfully deleted.',
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @Delete(':categoryId')
    async delete(@Param() params) {
        return this.categoryService.delete(params.categoryId);
    }

}
